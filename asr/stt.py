# coding: utf8
from __future__ import absolute_import
from aip import AipSpeech
from asr.configs import AUDIO_OUTPUT, AUDIO_FORMAT, APP_ID, API_KEY, SECRET_KEY, TIME
from asr.speech import audio_record
from asr.recognition import aip_get_asrresult, asr_abc_result
from asr.awake import up_ps_audio
from asr.wakeup_snowboy import wakeup_waiting
from yiwa.log import Log
import wave
import pyaudio
import time
import os
from yiwa.settings import BASE_DIR

DETECT_DONG = os.path.join(BASE_DIR, "asr", "resources/dong.wav")
logger = Log().logger


def listening():
    """监听麦克风"""
    print("Please say the command")
    audio_record(AUDIO_OUTPUT, TIME)
    # asr_result = aip_get_asrresult(AipSpeech(APP_ID, API_KEY, SECRET_KEY),
    #                                AUDIO_OUTPUT,
    #                                AUDIO_FORMAT)
    asr_result = asr_abc_result()
    return "".join(asr_result) if asr_result else None


def wakeup(keywords: list):
    """唤醒"""
    print("Please wake me up")
    # audio_record(AUDIO_OUTPUT, TIME)
    # word = up_ps_audio(AUDIO_OUTPUT)
    word = "伊瓦" if wakeup_waiting() else "-"
    logger.info(f"唤醒>>> {word}")
    return word, (str(word) in keywords)


def play_audio_file(fname=DETECT_DONG):
    """播放wav文件声音文件"""
    ding_wav = wave.open(fname, 'rb')
    ding_data = ding_wav.readframes(ding_wav.getnframes())
    audio = pyaudio.PyAudio()
    stream_out = audio.open(
        format=audio.get_format_from_width(ding_wav.getsampwidth()),
        channels=ding_wav.getnchannels(),
        rate=ding_wav.getframerate(), input=False, output=True)
    stream_out.start_stream()
    stream_out.write(ding_data)
    time.sleep(0.2)
    stream_out.stop_stream()
    stream_out.close()
    audio.terminate()


if __name__ == "__main__":
    print(listening())
    play_audio_file()
