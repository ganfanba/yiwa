# coding: utf8

""""""

import platform
from importlib import import_module

systems = {"Darwin": "osx", "Linux": ["rpi", "ubuntu"]}
system_names = systems.get(platform.system(), [])
if not isinstance(system_names, list):
    system_names = [str(system_names)]
for system in system_names:
    try:
        snowboydetect = import_module(f"snowboy.{system}.snowboydetect")
    except Exception as e:
        print(f"[{system}] import snowboy error {e}")
