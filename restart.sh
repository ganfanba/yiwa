ps -ef | grep web.py | awk '{print $2}' | xargs kill -9
nohup python3 ~/yiwa/web.py > nohup_web.out 2>&1 &

ps -ef | grep yiwa.py | awk '{print $2}' | xargs kill -9
nohup python3 ~/yiwa/yiwa.py > nohup_yiwa.out 2>&1 &