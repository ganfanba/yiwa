# coding: utf8

"""自律表格配置文件"""

APPID = "tetris"
APPNAME = "俄罗斯方块"
COMMANDS = {
    "俄罗斯方块": {
        "commands": ["俄罗斯", "俄罗斯方块", "俄罗斯方块游戏", "方块游戏"],
        "action": "/tetris/index",
        "global": 1
    },
    "开始": {
        "commands": ["开始"],
        "action": "apps.tetris.index.start",
    },
    "重来": {
        "commands": ["重来"],
        "action": "apps.tetris.index.restart",
    },
    "左移": {
        "commands": ["左", "左边", "左移", "往左", "向左移动"],
        "action": "apps.tetris.index.left",
    },
    "右移": {
        "commands": ["右", "右边", "右移", "往右", "向右移动"],
        "action": "apps.tetris.index.right",
    },
    "旋转": {
        "commands": ["旋转"],
        "action": "apps.tetris.index.rotate",
    },
    "落下": {
        "commands": ["好了", "下降", "降落", "落下", "最底下"],
        "action": "apps.tetris.index.fall",
    },
}
