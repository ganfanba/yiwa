# coding: utf-8

"""自定义字符串处理工具"""

import hashlib
import uuid
import re
from util import add_decorator_for_public_method

add_decorator_for_public_method(staticmethod)


class StringUtil(object):
    """字符串处理类"""

    def conceal(_str) -> str:
        """
        遮掩字符串, 中间部分用*号代替
        :param _str: 要处理的字符串
        :return: 最长三位的字符串, 如果***表示失败
        """
        try:
            value_str = str(_str)
            if len(value_str) > 2:
                return f"{value_str[0]}*{value_str[-1]}"
            elif len(value_str) > 0:
                return f"*{value_str[-1]}"
            else:
                return value_str
        except Exception as ex:
            return "***"

    @staticmethod
    def md5(_str: str, upper=False):
        """
        对字符串md5
        :param _str: 要处理的字符串
        :param upper: 是否全转为大写
        :return: md5处理后字符串
        """
        md5str = hashlib.md5(_str.encode()).hexdigest()
        if upper:
            return md5str.upper()
        return md5str

    @staticmethod
    def hash(_str: str, upper=False):
        """
        对字符串md5
        :param _str: 要处理的字符串
        :param upper: 是否全转为大写
        :return: md5处理后字符串
        """
        sha256str = hashlib.sha256(_str.encode()).hexdigest()
        if upper:
            return sha256str.upper()
        return sha256str

    @staticmethod
    def uuid1(sep=False):
        """
        生成唯一uuid1码
        :param sep: 包含分隔符
        :return: uuid码，不带分隔符长度32，带分隔符36
        """
        uuid_str = uuid.uuid1().__str__()
        if sep:
            return uuid_str
        return uuid_str.replace("-", "")

    @staticmethod
    def uuid5(sep=False, namespace=""):
        """
        生成唯一uuid5码
        :param sep: 包含分隔符
        :param namespace:   命名空间字符串，
        :return: uuid码，不带分隔符长度32，带分隔符36
        """
        uuid_str = uuid.uuid3(uuid.NAMESPACE_DNS, namespace).__str__()
        if sep:
            return uuid_str
        return uuid_str.replace("-", "")

    @staticmethod
    def remove_punctuation(text):
        """只保留中文、大小写字母和阿拉伯数字"""
        if not text or not isinstance(text, str):
            return ""
        reg = "[^0-9A-Za-z\u4e00-\u9fa5]"
        text = re.sub(reg, "", text)
        # 去除重复汉字
        p = re.compile(r"([\u4e00-\u9fa5])(\1+)")
        return p.sub(r"\1", text)

    @staticmethod
    def same_word(text: str) -> bool:
        """相同字词"""
        return set(text).__len__() <= 1

    @staticmethod
    def chn_number(text: str) -> int:
        """找出中文数字，并转成D单个阿拉伯数字，返回最大数字"""
        numbers = {"一": 1, "两": 2, "二": 2, "三": 3, "四": 4, "五": 5,
                   "六": 6, "七": 7, "八": 8, "九": 9, "十": 10}
        integers = [numbers.get(s, 0) for s in text]
        if not integers:
            return 1
        integer = max(integers)
        return integer if integer > 0 else 1


if __name__ == "__main__":
    pass
