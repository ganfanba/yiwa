# coding: utf-8

"""内存全局数据"""

import memcache


class SystemCache(object):
    """系统缓存"""

    def __init__(self):
        super(SystemCache, self).__init__()
        self.mc = memcache.Client(['127.0.0.1:11211'], debug=False)

    def get_set(self, name, value, default):
        """获取或设置缓存值"""
        if value is None:
            value = self.mc.get(name)
            return value if value else default
        self.mc.set(name, value)
        return value

    def status(self, value=None):
        return self.get_set("status", value, 0)

    def caption(self, value=None):
        return self.get_set("caption", value, "请唤醒我吧：")

    def listening(self, value=None):
        return self.get_set("listening", value, 0)

    def info(self, value=None):
        return self.get_set("info", value, "睡眠待命")

    def stt(self, value=None):
        return self.get_set("stt", value, "")

    def say(self, value=None):
        return self.get_set("say", value, "")

    def refresh(self, value=None):
        """刷新页面标注"""
        return self.get_set("refresh", value, 0)

    def set_yiwa_table(self, **kwargs):
        """设置yiwa表的缓存"""
        for name, value in kwargs.items():
            self.__getattribute__(name)(value)

    def get_yiwa_table(self, *args):
        if not args:
            return {}
        return {name: self.__getattribute__(name)()
                for name in args}


if __name__ == "__main__":
    sc = SystemCache()
    print(sc.status())
    print(sc.set_yiwa_table(status=0, caption="", listening=0, info="", stt=""))
    print(sc.get_yiwa_table("status", "caption", "listening", "info", "stt"))
